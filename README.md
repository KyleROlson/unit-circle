# Unit Circle Game
###### Started 4/21/2018

  
### Project Description:
This is an interactive unit circle. Once completed, it will feature multiple modes.
* Type Unit Circle, the user must type in the correct answer to blanks in the unit circle.
* Multiple Choice Unit Circle, the user must select the correct answer to blanks from 4 different choices.
* Type Flash Cards, the user will be presented with a "flash card" of sine or cosine and must type in the answer.

It will utilize a distinct UI with graphics that look like they were pencilled into a notebook.

### Task List:
* Create UI
* Write JavaScript to check answers.
* Wire Javascript to Buttons
* Create High Score Database
* Mobile apps?